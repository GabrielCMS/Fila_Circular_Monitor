#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define tam 15

typedef struct Monitor{
  char modelo[15];
  char resolucao[15];
  float preco;
}Monitor;

typedef struct Fila{
  Monitor fila[tam];
  int inicio, fim, qtdelemento;
}Fila;

void iniciarFila(Fila *f){
  f->inicio = 0;
  f->qtdelemento = 0;
  f->fim = -1;
}

int verifica(Fila *f){
  return (f->qtdelemento == tam);
}

void insere(Fila *f, char modelo[], char resolucao[], float preco){
  if(verifica(f)){
    printf("Numero maximo de Monitores cadastrados! \n");
    return;
  }

  if(f->fim == (tam - 1)){
    f->fim = -1;
  }

 
  Monitor newMonitor;
  strcpy(newMonitor.modelo, modelo);
  strcpy(newMonitor.resolucao, resolucao);
  newMonitor.preco = preco;
  f->fim++;
  f->fila[f->fim] = newMonitor;
  f->qtdelemento++;
  printf("Monitor Cadastrado!\n");
}

void remove(Fila *f){
  if(f->qtdelemento == 0){
    printf("Nao ha monitores Cadastrados!\n");
    return;
  }

  if(f->inicio == tam){
    f->inicio = 0;
  }

  f->inicio++;
  f->qtdelemento--;
  printf("Primeiro Monitor cadastrado foi Removido!\n");
}

void mostraPrimeiro(Fila *f){
  if(f->qtdelemento == 0){
    printf("Nao ha monitores Cadastrados!!\n");
  }else{
    printf("--------------- Dados do Monitor --------------- \n");
    printf("Modelo do monitor: %s\n", f->fila[f->inicio].modelo);
    printf("Resolucao do monitor: %s\n", f->fila[f->inicio].resolucao);
    printf("Preço do monitor: %f\n", f->fila[f->inicio].preco);
  }
}
